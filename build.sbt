name := """PlanDataMigration"""
organization := "ncinga"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.3"

libraryDependencies ++= Seq(
  guice,
  "org.pac4j" % "pac4j-mongo" % "3.8.3",
  "org.mongodb" % "mongo-java-driver" % "3.8.2",
  "org.mongodb" % "bson" % "3.4.2",
  "com.google.code.gson" % "gson" % "2.8.5",
  "com.fasterxml.jackson.module" % "jackson-module-guice" % "2.10.2",
  "com.googlecode.json-simple" % "json-simple" % "1.1.1"





)
