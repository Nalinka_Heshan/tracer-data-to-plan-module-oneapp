package controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import connection.Mongo;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.planService.PlanDataHandler;

public class PlanController extends Controller {
    private PlanDataHandler planDataHandler;
    Mongo mongo;
    public PlanController() {
        mongo = Mongo.getInstance();
        planDataHandler= new PlanDataHandler(mongo);
    }

    public Result readPlanData(Http.Request request) throws JsonProcessingException {

        JsonNode planData= request.body().asJson();
        String subjectKey= planData.get("subject_key").asText();
        String user=planData.get("user").asText();
        String source=planData.get("source").asText();
        String planName=planData.get("plan_name").asText();
        String filePath=planData.get("file_path").asText();

        planDataHandler.insertPlan(filePath,planName,user,source,subjectKey);

        return ok("inserted.");
    }
}

