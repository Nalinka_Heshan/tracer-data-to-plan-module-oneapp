package services.readRowDataService;

import com.google.gson.*;
import org.bson.Document;

import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ReadEmProData {
    private String pathOfJSONFile;
    private ArrayList<Document> records;

    public ReadEmProData(String pathOfJSONFile) {
        this.pathOfJSONFile = pathOfJSONFile;
        records = new ArrayList<>();
    }

    public ArrayList<Document> getRecordDocuments() {
        readRawDataFile();
        return records;
    }


    private void readRawDataFile(){


        try {
            Validator validateRawData= new Validator();
            Gson gson = new Gson();
            Reader reader = Files.newBufferedReader(Paths.get(pathOfJSONFile));
            JsonObject json = gson.fromJson(reader, JsonObject.class);
            JsonObject content = gson.fromJson(json.toString(), JsonObject.class);
            JsonObject aggregations = (JsonObject) content.get("aggregations");
            JsonObject factory = (JsonObject) aggregations.get("factory");
            JsonArray factoryBuckets = factory.getAsJsonArray("buckets");

            JsonObject factoryObject = factoryBuckets.get(0).getAsJsonObject();
            String facName= factoryObject.get("key").getAsString();
            JsonObject unit = (JsonObject) factoryObject.get("unit");
            JsonArray unitBuckets = unit.getAsJsonArray("buckets");

            JsonObject unitObject = unitBuckets.get(0).getAsJsonObject();
            String unitName= unitObject.get("key").getAsString();
            JsonObject floor = (JsonObject) unitObject.get("floor");
            JsonArray floorBuckets = floor.getAsJsonArray("buckets");

            for (JsonElement section : floorBuckets) {

                JsonObject sectionObject = section.getAsJsonObject();
                String floorName= sectionObject.get("key").getAsString();
                JsonObject line = (JsonObject) sectionObject.get("line");
                JsonArray lineBuckets = line.getAsJsonArray("buckets");


                for (JsonElement lineOb: lineBuckets) {

                    JsonObject lineObject = lineOb.getAsJsonObject();
                    String lineName= lineObject.get("key").getAsString();
                    JsonObject date = (JsonObject) lineObject.get("date");
                    JsonArray dateBucket = date.getAsJsonArray("buckets");

                    if(!(validateRawData.validateLines(lineName))){
                        continue;
                    }

                    for (JsonElement dateOb: dateBucket){

                        JsonObject dateObject = dateOb.getAsJsonObject();
                        long dateKey= dateObject.get("key").getAsLong();
                        String dateKeyAsString=dateObject.get("key_as_string").getAsString();
                        JsonObject buyer = (JsonObject) dateObject.get("buyer");
                        JsonArray buyerBucket= buyer.getAsJsonArray("buckets");

                        for (JsonElement buyerOb: buyerBucket){

                            JsonObject buyerObject= buyerOb.getAsJsonObject();
                            String planItemBuyerValue= buyerObject.get("key").getAsString();
                            JsonObject subRefNo= (JsonObject) buyerObject.get("subrefno");
                            JsonArray subRefNoBucket= subRefNo.getAsJsonArray("buckets");

                            for (JsonElement subRefOb: subRefNoBucket){

                                JsonObject subRefNoObject= subRefOb.getAsJsonObject();
                                String subRefNoKey= subRefNoObject.get("key").getAsString();
                                JsonObject so= (JsonObject) subRefNoObject.get("so");
                                JsonArray soBucket= so.getAsJsonArray("buckets");

                                for (JsonElement soOb: soBucket){

                                    JsonObject soObject= soOb.getAsJsonObject();
                                    String planItemSoValue= soObject.get("key").getAsString();
                                    JsonObject li= (JsonObject) soObject.get("li");
                                    JsonArray liBucket= li.getAsJsonArray("buckets");

                                    for (JsonElement liOb: liBucket){

                                        JsonObject liObject= liOb.getAsJsonObject();
                                        String liKey = liObject.get("key").getAsString();
                                        JsonObject itemName= (JsonObject) liObject.get("itemname");
                                        JsonArray itemNameBucket= itemName.getAsJsonArray("buckets");

                                        for (JsonElement itemNameOb: itemNameBucket){

                                            JsonObject itemNameObject= itemNameOb.getAsJsonObject();
                                            String itemNameValue = itemNameObject.get("key").getAsString();
                                            JsonObject style= (JsonObject) itemNameObject.get("style");
                                            JsonArray styleBucket= style.getAsJsonArray("buckets");

                                            for (JsonElement styleOb: styleBucket){

                                                JsonObject styleObject= styleOb.getAsJsonObject();
                                                String styleValue = styleObject.get("key").getAsString();
                                                JsonObject dt= (JsonObject) styleObject.get("dt");
                                                JsonArray dtBucket= dt.getAsJsonArray("buckets");

                                                for (JsonElement dtOb: dtBucket){

                                                    JsonObject dtObject= dtOb.getAsJsonObject();
                                                    String dateTimeAsValue = dtObject.get("key").getAsString();
                                                    String dateTimeAsString= dtObject.get("key_as_string").getAsString();
                                                    float efficiency = ((JsonObject) dtObject.get("efficiency")).get("value").getAsFloat();
                                                    int helpers = ((JsonObject) dtObject.get("helpers")).get("value").getAsInt();
                                                    int operators = ((JsonObject) dtObject.get("operators")).get("value").getAsInt();
                                                    int qty = ((JsonObject) dtObject.get("qty")).get("value").getAsInt();
                                                    float smv = ((JsonObject) dtObject.get("smv")).get("value").getAsFloat();

                                                    Document record= new Document();

                                                    record.put("factory_name",facName);
                                                    record.put("unit_name",unitName);
                                                    record.put("floor_name",floorName);
                                                    record.put("line_no",lineName);
                                                    record.put("date_as_value",dateKey);
                                                    record.put("date_as_string",dateKeyAsString);
                                                    record.put("Buyer",planItemBuyerValue);
                                                    record.put("subrefno",subRefNoKey);
                                                    record.put("SO Number",planItemSoValue);
                                                    record.put("li_value",liKey);
                                                    record.put("item_name_value",itemNameValue);
                                                    record.put("Style No",styleValue);
                                                    record.put("Planned Date",dateTimeAsValue);
                                                    record.put("date_time_as_string",dateTimeAsString);
                                                    record.put("Planned Eff.",efficiency);
                                                    record.put("Helper",helpers);
                                                    record.put("Operator",operators);
                                                    record.put("Day Target",qty);
                                                    record.put("SMV",smv);
                                                    record.put("PO Number",null);
                                                    record.put("Product Type",null);
                                                    record.put("Planned SAH",null);
                                                    record.put("working_hours",null);
                                                    record.put("Sew Floor Name",setFloor(lineName));
                                                    record.put("Line no",getKLineValue(validateRawData.getLineNumber(lineName)));
                                                    record.put("Unit Name",setUnitName(floorName));

                                                    records.add(record);




                                                }
                                            }

                                        }
                                    }

                                }
                            }
                        }
                    }
                }
            }

            reader.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
    private String setFloor(String lineNumber){
        Validator validator= new Validator();
        int number= validator.getLineNumber(lineNumber);
        if(number<4){
            return null;

        }
        else if(number<9){
            return "Second Floor - A";
        }
        else if(number<14){
            return "Second Floor - B";
        }
        else{
            return null;
        }
    }
    private String setUnitName(String floorName){
        if(floorName.equalsIgnoreCase("Empro_12/2")){
            return "PEMP 12-2";
        }
        return null;

    }
    private String getKLineValue(int number){
        String v="Line ";
        if(number<10){
            v+="0"+ number;
        }
        else{
            v+=Integer.toString(number);
        }
        return v;
    }
/*    public JsonArray getRowDataAsJSONArray(){
        Gson gson = new Gson();
        JsonArray jsonRecords= new JsonArray();

        readRawDataFile();

        for(HashMap<?,?> record: records){

            String json = gson.toJson(record);
            JsonObject jsonObject = new JsonParser().parse(json).getAsJsonObject();
            jsonRecords.add(jsonObject);
        }
        return jsonRecords;
    }*/
}
