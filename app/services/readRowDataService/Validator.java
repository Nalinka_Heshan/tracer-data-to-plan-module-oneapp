package services.readRowDataService;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Validator {
    public boolean validateLines(String line){
        int number= getLineNumber(line);
        return number > -1 && number < 20;
    }
    // not required
    public boolean validateFloor(String floor){
        List<String> list = new LinkedList<String>(Arrays.asList("Empro_12/2".toLowerCase(),"Empro_12/2".toLowerCase(),"Empro_13".toLowerCase()));
        return list.contains(floor.toLowerCase());
    }
    public int getLineNumber(String line){
        if(line.equalsIgnoreCase("line1c")){
            return 0;
        }
        if((line.length()>6)|| line.length()<5){
            return -1;
        }
        else if(line.toLowerCase().contains("line".toLowerCase())){
            String value= line.split("line")[1];
            if(value.matches("[0-9]+")){
                return Integer.parseInt(value);
            }
            else {
                return -1;
            }
        }
        else {
            return -1;
        }

    }
}
