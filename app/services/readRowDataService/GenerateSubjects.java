package services.readRowDataService;

import org.bson.Document;

import java.util.ArrayList;
import java.util.HashMap;

public class GenerateSubjects {
    private HashMap<String,String> subjects;
    private int number;
    private ArrayList<Document> requiredSubjects;

    public GenerateSubjects() {
        subjects= new HashMap<>();
        subjects.put("floor2-sewing-section1","ind-pemp-p122-plant1-floor2-sewing-section1");
        subjects.put("floor2-sewing-section2","ind-pemp-p122-plant1-floor2-sewing-section2");
        subjects.put("floor2-finishing-section1","ind-pemp-p122-plant1-floor2-finishing-section1");
        subjects.put("floor2-finishing-section2","ind-pemp-p122-plant1-floor2-finishing-section2");
        subjects.put("floor1-sewing","ind-pemp-p122-plant1-floor1-sewing-section1");
        subjects.put("floor1-finishing","ind-pemp-p122-plant1-floor1-finishing-section2");
        subjects.put("floor3-sewing","ind-pemp-p122-plant1-floor3-sewing-section1");
        subjects.put("floor3-finishing","ind-pemp-p122-plant1-floor3-finishing-section2");

        requiredSubjects= new ArrayList<>();
    }
    public ArrayList<Document> getSubjectsByLineNumber(String lineNo){
        Validator validator= new Validator();
        number= validator.getLineNumber(lineNo);
        if(validator.validateLines(lineNo)){
            if(number<4){
                setSubject("floor1-sewing",lineNo);
                setSubject("floor1-finishing",lineNo);

            }
            else if(number<9){
                setSubject("floor2-sewing-section1",lineNo);
                setSubject("floor2-finishing-section1",lineNo);
            }
            else if( number<14){
                setSubject("floor2-sewing-section2",lineNo);
                setSubject("floor2-sewing-section2",lineNo);


            }
            else if( number<20){
                setSubject("floor3-sewing",lineNo);
                setSubject("floor3-finishing",lineNo);

            }
        }
        return requiredSubjects;
    }
    private void setSubject(String key, String lineNo){
        setSubjectsUntilStations(key, lineNo);
        setSubjectsUnitLine(key, lineNo);

    }
    private void setSubjectsUntilStations(String key, String lineNo){
        String k1= subjects.get(key)+"-"+lineNo+"-station1";
        String k2= subjects.get(key)+"-"+lineNo+"-station2";
        String v= getKValue();
        createAndSaveDocument(k1,v);
        createAndSaveDocument(k2,v);

    }
    private void setSubjectsUnitLine(String key, String lineNo){
        String k= subjects.get(key)+"-"+lineNo;
        String v= getKValue();
        createAndSaveDocument(k,v);
    }
    private String getKValue(){
        String v="Line ";
        if(number<10){
            v+="0"+ number;
        }
        else{
            v+=Integer.toString(number);
        }
        return v;
    }
    private void createAndSaveDocument(String kValue, String vValue){
        Document object= new Document();
        object.put("k",kValue);
        object.put("v",vValue);
        requiredSubjects.add(object);
    }
}
