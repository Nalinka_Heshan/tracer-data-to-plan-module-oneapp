package services.planService.read;

import org.bson.Document;

import java.util.ArrayList;

public class PlanMapping {
    private String subjectKey;
    private String planName;
    private String planType;
    private Document subjects;
    private ArrayList<Document> orgArgs;
    private ArrayList<Document> planItem;
    private Document planDatetime;
    private Document planValues;
    private Document axillary;
    private Document filter;
    private ArrayList<Document> planValidation;
    private Document order;
    public String getSubjectKey() {
        return subjectKey;
    }

    public void setSubjectKey(String subjectKey) {
        this.subjectKey = subjectKey;
    }

    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public Document getSubjects() {
        return subjects;
    }

    public void setSubjects(Document subjects) {
        this.subjects = subjects;
    }

    public ArrayList<Document> getOrgArgs() {
        return orgArgs;
    }

    public void setOrgArgs(ArrayList<Document> orgArgs) {
        this.orgArgs = orgArgs;
    }

    public ArrayList<Document> getPlanItem() {
        return planItem;
    }

    public void setPlanItem(ArrayList<Document> planItem) {
        this.planItem = planItem;
    }

    public Document getPlanDatetime() {
        return planDatetime;
    }

    public void setPlanDatetime(Document planDatetime) {
        this.planDatetime = planDatetime;
    }

    public Document getPlanValues() {
        return planValues;
    }

    public void setPlanValues(Document planValues) {
        this.planValues = planValues;
    }

    public Document getAxillary() {
        return axillary;
    }

    public void setAxillary(Document axillary) {
        this.axillary = axillary;
    }

    public Document getFilter() {
        return filter;
    }

    public void setFilter(Document filter) {
        this.filter = filter;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public void setPlanValidation(ArrayList<Document> planValidation) {
        this.planValidation = planValidation;
    }

    public ArrayList<Document> getPlanValidation() {
        return planValidation;
    }

    public void setOrder(Document order) {
        this.order = order;
    }

    public Document getOrder() {
        return order;
    }

}
