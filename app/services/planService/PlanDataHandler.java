package services.planService;


import com.fasterxml.jackson.core.JsonProcessingException;
import connection.MasterData;
import connection.Mongo;
import org.bson.Document;
import org.json.simple.JSONObject;
import services.planService.map_plan.Mapping;
import services.planService.map_plan.Plan;
import services.planService.read.PlanMapping;
import services.planService.save.PlanDao;
import services.readRowDataService.ReadEmProData;


import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.HashMap;

public class PlanDataHandler {
    MasterData masterData;
    String timeZone="Asia/Colombo";
    Mongo mongo;
    boolean clearAndInsert=true;
    private HashMap<String, ZoneId> zoneIdMap = new HashMap<>();

    public PlanDataHandler(Mongo mongo) {
        masterData = new MasterData(mongo);
        this.mongo=mongo;
        this.setZoneIds();
    }

    public Plan insertPlan(String pathToInputFile, String planName, String user, String source, String subjectKey) throws JsonProcessingException {


        PlanMapping planMapping = getPlanMapping(subjectKey, planName);
        Document filterDetails = planMapping.getFilter();

        ReadEmProData data = new ReadEmProData(pathToInputFile);
        ArrayList<Document> records = data.getRecordDocuments();

        Mapping insertPlan = new Mapping();
        Plan plan = insertPlan.convertToPlan(records, planMapping, user, source, subjectKey);

        PlanDao planDao= new PlanDao(mongo);
        planDao.insertPlan(plan.getPlanRecords(),this.createPlanCollectionName(subjectKey), this.getUploadBoundary(filterDetails, subjectKey), planName, clearAndInsert, timeZone);

        return plan;

    }

    public PlanMapping getPlanMapping(String subjectKey, String planName) {

        HashMap<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("meta_name", planName);
        parameterMap.put("subject_key", subjectKey);
        JSONObject result = masterData.getMasterData(subjectKey, "get_plan_mapping", parameterMap);
        Document planMappingDoc = (Document) ((ArrayList) result.get("result")).get(0);
        if (planMappingDoc != null) {
            Document metaData = (Document) planMappingDoc.get("meta_data");
            if (metaData != null) {
                Document mapping = (Document) metaData.get("mapping");

                if (planMappingDoc != null) {
                    PlanMapping planMapping = new PlanMapping();
                    planMapping.setSubjectKey(subjectKey);
                    planMapping.setPlanName(planName);
                    planMapping.setPlanType(metaData.get("plan_type") != null ? metaData.getString("plan_type") : "default_plan");
                    planMapping.setSubjects((Document) mapping.get("subjects"));
                    planMapping.setOrgArgs((ArrayList<Document>) mapping.get("org_args"));
                    planMapping.setPlanItem((ArrayList<Document>) mapping.get("plan_item"));
                    planMapping.setPlanDatetime((Document) mapping.get("plan_datetime"));
                    planMapping.setPlanValues((Document) mapping.get("plan_values"));
                    planMapping.setAxillary((Document) mapping.get("axillary"));
                    planMapping.setFilter((Document) metaData.get("filter"));
                    if (mapping.containsKey("plan_validation")) {
                        planMapping.setPlanValidation((ArrayList<Document>) mapping.get("plan_validation"));
                    }
                    if (mapping.containsKey("order")) {
                        planMapping.setOrder((Document) mapping.get("order"));
                    }
                    return planMapping;
                }
            }
        }
        return null;
    }
    private String createPlanCollectionName(String subjectKey) {
        subjectKey = subjectKey.replace('-', '_');
        return subjectKey + "_plan";
    }
    private long getUploadBoundary(Document filterDetails, String subjectKey) {
        int days = filterDetails != null ? (filterDetails.get("days") != null ? filterDetails.getInteger("days") : 0) : 0;
        int dayStartMin = filterDetails != null ? (filterDetails.get("day_start_min") != null ? filterDetails.getInteger("day_start_min") : 0) : 0;
        Instant instant = Instant.now();
        ZonedDateTime zonedDateTime = instant.atZone(this.getZoneId(subjectKey));
        LocalDateTime todayStart = zonedDateTime.toLocalDate().atStartOfDay();

        LocalDateTime cutOffTime = todayStart.plusMinutes(dayStartMin);
        LocalDateTime boundaryDate = todayStart.plusDays(days);

        if (cutOffTime.atZone(this.getZoneId(subjectKey)).toEpochSecond() > instant.getEpochSecond()) {
            boundaryDate = boundaryDate.minusDays(1);
        }

        long epochSecond = boundaryDate.atZone(this.getZoneId(subjectKey)).toEpochSecond();
        return epochSecond * 1000;
    }
    private ZoneId getZoneId(String subjectKey) {
        return zoneIdMap.get(subjectKey) != null ? zoneIdMap.get(subjectKey) : zoneIdMap.get("default");
    }

    private void setZoneIds() {
        zoneIdMap.put("sgt-sublime", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("meg-sgt", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("test-fact1", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("ant-aal-aepz", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("ncsmeg_sgt", ZoneId.of("Asia/Colombo"));
        zoneIdMap.put("scl-ktn", ZoneId.of("Asia/Colombo"));
        zoneIdMap.put("default", ZoneId.of("Asia/Colombo"));
        zoneIdMap.put("sqg-brc-ftry1", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("psg-tal", ZoneId.of("Asia/Dhaka"));
    }
}

