package services.planService.save;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import connection.Mongo;
import org.bson.Document;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlanDao {
    protected HashMap<String, MongoDatabase> databaseMap;

    public PlanDao(Mongo mongo) {
        this.databaseMap = mongo.databaseMap;
    }

    public void insertPlan(ArrayList<Document> plan, String collectionName, long uploadTimeBoundary, String planName, boolean clearAndInsert, String  timeZone) {
        List<Document> distictSubjectList=new ArrayList<>();
        if(plan!=null && !plan.isEmpty()) {
            for (int i=0;i<plan.size();i++) {
                List<Document> subjects=(List)plan.get(i).get("subjects");
                for (Document doc:subjects) {
                    if(!distictSubjectList.contains(doc)){
                        distictSubjectList.add(doc);
                    }
                }
            }
        }

        ZoneId zoneId = ZoneId.of(timeZone);
        LocalDate localDate = LocalDate.now(zoneId);
        Map<String,ArrayList<Document>> map=separateData(plan, localDate);
        ArrayList<Document> currentDayData= map.get("curDate");
        ArrayList<Document> otherData= map.get("other");
        if(!currentDayData.isEmpty()){//current data logic
            MongoCollection<Document> collection = this.getDatabase("plan").getCollection(collectionName);
            List<Document> insertList=new ArrayList<Document>();
            for(Document document:currentDayData) {
                List<Document> subjects= (List<Document>) document.get("subjects");
                List<Document> distinctList=new ArrayList<>();
                for (Document doc:subjects) {
                    if(!distinctList.contains(doc)){
                        distinctList.add(doc);
                    }
                }
                String planDate=((Map)((Map)document.get("plan_datetime")).get("key")).get("date").toString();
                Document filter = new Document("plan_name", planName)
                        .append("plan_datetime.key.datetime", new Document("$gte", uploadTimeBoundary))
                        .append("plan_datetime.key.date", planDate)
                        .append("subjects", new Document("$in", distinctList));
//                System.out.println(filter.toJson(new JsonWriterSettings(JsonMode.SHELL, true)));
                collection.deleteMany(filter);
                insertList.add(document);
            }
            collection.insertMany(insertList);
        }
        if(!otherData.isEmpty()){
            MongoCollection<Document> collection = this.getDatabase(Mongo.DB_PLAN).getCollection(collectionName);
            Document filter = new Document("plan_name", planName)
                    .append("plan_datetime.key.datetime", new Document("$gte", uploadTimeBoundary))
                    .append("plan_datetime.key.date", new Document("$ne", localDate.toString()))
                    .append("subjects", new Document("$in", distictSubjectList));
            if (clearAndInsert) {
//                System.out.println(filter.toJson(new JsonWriterSettings(JsonMode.SHELL, true)));
                collection.deleteMany(filter);
            }
            collection.insertMany(otherData);
        }
        if(!otherData.isEmpty()){
            MongoCollection<Document> collection = this.getDatabase(Mongo.DB_PLAN).getCollection(collectionName);
            Document filter = new Document("plan_name", planName)
                    .append("plan_datetime.key.datetime", new Document("$lte", uploadTimeBoundary))
                    .append("plan_datetime.key.date", new Document("$ne", localDate.toString()))
                    .append("subjects", new Document("$in", distictSubjectList));
            if (clearAndInsert) {
//                System.out.println(filter.toJson(new JsonWriterSettings(JsonMode.SHELL, true)));
                collection.deleteMany(filter);
            }
            collection.insertMany(otherData);
        }
    }
    private Map<String,ArrayList<Document>> separateData(ArrayList<Document> plan, LocalDate curDate) {
        Map<String,ArrayList<Document>> result=new HashMap<>();
        ArrayList<Document> resultCurDate=new ArrayList<>();
        ArrayList<Document> resultOther=new ArrayList<>();
        if(plan!=null && !plan.isEmpty()){
            for(Document document:plan){
                String planDate=((Map)((Map)document.get("plan_datetime")).get("key")).get("date").toString();
                LocalDate planLocalDate = LocalDate.parse(planDate);
                if(planLocalDate.equals(curDate)){
                    resultCurDate.add(document);
                }else{
                    resultOther.add(document);
                }
            }
        }
        result.put("curDate",resultCurDate);
        result.put("other",resultOther);
        return result;
    }
    protected MongoDatabase getDatabase(String databaseName){
        return databaseMap.get(databaseName);
    }
}
