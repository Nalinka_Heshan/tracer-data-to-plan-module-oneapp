package services.planService.map_plan;

import org.bson.Document;
import services.planService.read.PlanMapping;

import javax.inject.Singleton;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;

@Singleton
public class Mapping {
    private HashMap<String, ZoneId> zoneIdMap = new HashMap<>();

    public Mapping() {
        this.setZoneIds();
    }

    public Plan convertToPlan(ArrayList<Document> inputDocuments, PlanMapping planMapping, String user, String source, String subjectKey) {
        ArrayList<Document> planRecords = new ArrayList<>();
        ArrayList<Document> errors = new ArrayList<>();
        ArrayList<Document> warnings = new ArrayList<>();
        ArrayList<Document> allOrderData = null;
        for (Document input : inputDocuments) {
            PlanItem planItem = this.createPlanItem(input, planMapping, user, source, subjectKey, allOrderData);
            planRecords.add(planItem.toDocument());
        }
        return new Plan(planRecords, errors, warnings);

    }


    private PlanItem createPlanItem(Document inputRow, PlanMapping planMapping, String user, String source, String subjectKey, ArrayList<Document> allOrderData) {
        PlanItemBuilder planItemBuilder = new PlanItemBuilder(this.getZoneId(subjectKey));

        //ArrayList<Document> organization = this.getOrganization(planMapping.getSubjectKey());
        ArrayList<Document> allSubjects = null;
        return planItemBuilder.build(inputRow, planMapping, allSubjects, user, source, allOrderData);
    }

    private ZoneId getZoneId(String subjectKey) {
        return zoneIdMap.get(subjectKey) != null ? zoneIdMap.get(subjectKey) : zoneIdMap.get("default");
    }

    private void setZoneIds() {
        zoneIdMap.put("sgt-sublime", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("meg-sgt", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("test-fact1", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("ant-aal-aepz", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("ncsmeg_sgt", ZoneId.of("Asia/Colombo"));
        zoneIdMap.put("scl-ktn", ZoneId.of("Asia/Colombo"));
        zoneIdMap.put("default", ZoneId.of("Asia/Colombo"));
        zoneIdMap.put("sqg-brc-ftry1", ZoneId.of("Asia/Dhaka"));
        zoneIdMap.put("psg-tal", ZoneId.of("Asia/Dhaka"));
    }
}
