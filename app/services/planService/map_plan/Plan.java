package services.planService.map_plan;

import org.bson.Document;

import java.util.ArrayList;

public class Plan {
    private ArrayList<Document> planRecords = new ArrayList<>();
    private ArrayList<Document> errors = new ArrayList<>();
    private ArrayList<Document> warnings = new ArrayList<>();

    public Plan(ArrayList<Document> planRecords, ArrayList<Document> errors, ArrayList<Document> warnings) {
        this.planRecords = planRecords;
        this.errors = errors;
        this.warnings = warnings;
    }

    public void addPlanRecord(Document planRecord) {
        this.planRecords.add(planRecord);
    }

    public void addError(Document error) {
        this.errors.add(error);
    }

    public void addWarning(Document warning) {
        this.warnings.add(warning);
    }

    public ArrayList<Document> getPlanRecords() {
        return planRecords;
    }

    public ArrayList<Document> getErrors() {
        return errors;
    }

    public ArrayList<Document> getWarnings() {
        return warnings;
    }


}
