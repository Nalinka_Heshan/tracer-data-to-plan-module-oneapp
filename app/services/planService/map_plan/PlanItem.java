package services.planService.map_plan;

import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;

public class PlanItem {
    private String planName;
    private String planType;
    private String subjectKey;
    private String user;
    private ArrayList<Document> subjects;
    private ArrayList<Document> planItem;
    private long datetime;
    private Document planDatetime;
    private Document planValues;
    private Document axillary;
    private ArrayList<Document> errors;
    private ArrayList<Document> warnings;
    private String source;
    private Document order;

    public PlanItem() {
        this.datetime = new Date().getTime();
    }


    public String getPlanName() {
        return planName;
    }

    public void setPlanName(String planName) {
        this.planName = planName;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public String getSubjectKey() {
        return subjectKey;
    }

    public void setSubjectKey(String subjectKey) {
        this.subjectKey = subjectKey;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public ArrayList<Document> getSubjects() {
        return subjects;
    }

    public void setSubjects(ArrayList<Document> subjects) {
        this.subjects = subjects;
    }

    public ArrayList<Document> getPlanItem() {
        return planItem;
    }

    public void setPlanItem(ArrayList<Document> planItem) {
        this.planItem = planItem;
    }

    public long getDatetime() {
        return datetime;
    }

    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public Document getPlanDatetime() {
        return planDatetime;
    }

    public void setPlanDatetime(Document planDatetime) {
        this.planDatetime = planDatetime;
    }

    public Document getPlanValues() {
        return planValues;
    }

    public void setPlanValues(Document planValues) {
        this.planValues = planValues;
    }

    public Document getAxillary() {
        return axillary;
    }

    public void setAxillary(Document axillary) {
        this.axillary = axillary;
    }

    public ArrayList<Document> getErrors() {
        return errors;
    }

    public void setErrors(ArrayList<Document> errors) {
        this.errors = errors;
    }

    public ArrayList<Document> getWarnings() {
        return warnings;
    }

    public void setWarnings(ArrayList<Document> warnings) {
        this.warnings = warnings;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public boolean isValid() {
        if (errors != null && errors.size() > 0) {
            return false;
        } else {
            return true;
        }
    }

    public Document toDocument() {
        Document document = new Document();
        document.put("subject_key", this.subjectKey);
        document.put("plan_name", this.planName);
        document.put("plan_type", this.planType);
        document.put("datetime", this.datetime);

        if (this.subjects != null) {
            document.put("subjects", this.subjects);
        }
        if (this.planItem != null) {
            document.put("plan_item", this.planItem);
        }
        if (this.planDatetime != null) {
            document.put("plan_datetime", this.planDatetime);
        }

        if (this.planValues != null) {
            document.put("plan_values", this.planValues);
        }
        if (this.axillary != null) {
            document.put("axillary", this.axillary);
        }
        if (this.order != null) {
            document.put("order", this.order);
        }

        document.put("user", this.user);
        document.put("source", this.source);
        return document;
    }

    public void setOrder(Document order) {
        this.order = order;
    }

    public Document getOrder() {
        return order;
    }
}
