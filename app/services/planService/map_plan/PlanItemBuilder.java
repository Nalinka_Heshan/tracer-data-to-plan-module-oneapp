package services.planService.map_plan;


import org.bson.Document;
import services.planService.read.PlanMapping;
import services.readRowDataService.GenerateSubjects;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;

public class PlanItemBuilder {
    private ArrayList<Document> errors;
    private ArrayList<Document> warnings;
    private ZoneId zoneId;

    public static HashMap<String, String> validationMap = new HashMap<>();

    public PlanItemBuilder(ZoneId zoneId) {
        this.errors = new ArrayList<>();
        this.warnings = new ArrayList<>();
        this.zoneId = zoneId;
    }

    public PlanItem build(Document input, PlanMapping planMapping, ArrayList<Document> allSubjects, String user, String source, ArrayList<Document> allOrderData) {

        PlanItem planItem = new PlanItem();
        planItem.setPlanName(planMapping.getPlanName());
        planItem.setPlanType(planMapping.getPlanType());
        planItem.setSubjectKey(planMapping.getSubjectKey());
        planItem.setUser(user);
        planItem.setSubjects(this.getSubjects(input));
        planItem.setPlanItem(this.createPlanItem(input, planMapping));
        planItem.setPlanDatetime(this.getPlanDatetimes(input, planMapping));
        planItem.setPlanValues(this.getPlanValues(input, planMapping));
        planItem.setAxillary(this.getAxillary(input, planMapping));
        //planItem.setOrder(this.getOrder(planItem,planMapping,rowNumber,allOrderData));
        planItem.setSource(source);

        planItem.setErrors(this.errors);
        planItem.setWarnings(this.warnings);

        return planItem;
    }

    private ArrayList<Document> getSubjects(Document record) {
        GenerateSubjects subject = new GenerateSubjects();
        String lineNo = record.get("line_no").toString();
        return subject.getSubjectsByLineNumber(lineNo);

    }

    private Document getPlanDatetimes(Document input, PlanMapping planMapping) {
        Document planDatetimes = new Document();
        Document planDatetimeMapping = planMapping.getPlanDatetime();
        for (String key : planDatetimeMapping.keySet()) {

            String displayName = planDatetimeMapping.getString(key);
            try {
                planDatetimes.put(key, this.createTimeObject(Long.parseLong(input.get(displayName).toString()), displayName));
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

        return planDatetimes.isEmpty() ? null : planDatetimes;
    }

    private Document createTimeObject(long datetime, String displayName) {

        DateTimeFormatter format1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime localDateTime =
                LocalDateTime.ofInstant(Instant.ofEpochMilli(datetime), this.zoneId);

        Document document = new Document();
        document.put("datetime", datetime);
        document.put("display_name", displayName);
        document.put("date", localDateTime.format(format1));

        return document;
    }

    private Document getPlanValues(Document input, PlanMapping planMapping) {
        Document planValues = new Document();
        Document planValueMapping = planMapping.getPlanValues();
        for (String key : planValueMapping.keySet()) {
            try {
                if (input.get(planValueMapping.getString(key)) != null) {
                    planValues.put(key, input.get(planValueMapping.getString(key)));
                } else {
                    planValues.put(key, "");
                }
            } catch (Exception ex) {

            }

        }
        return planValues.isEmpty() ? null : planValues;
    }

    private Document getAxillary(Document input, PlanMapping planMapping) {
        Document axillary = new Document();
        Document axillaryMapping = planMapping.getAxillary();
        for (String key : axillaryMapping.keySet()) {
            try {
                if (input.get(axillaryMapping.getString(key)) != null) {
                    axillary.put(key, input.get(axillaryMapping.getString(key)));
                } else {
                    axillary.put(key, null);
                }
            } catch (Exception ex) {

            }

        }
        return axillary.isEmpty() ? null : axillary;
    }

    private ArrayList<Document> createPlanItem(Document inputPlan, PlanMapping planMapping) {
        ArrayList<Document> planItem = new ArrayList<>();
        ArrayList<Document> planItemTemplate = planMapping.getPlanItem();

        for (Document document : planItemTemplate) {
            Document insert = new Document();
            insert.putAll(document);
            Object value = inputPlan.get(document.getString("v"));
            if (document.get("k").toString().equalsIgnoreCase("po") && inputPlan.get("PO Number") == null && inputPlan.get("SO Number") != null) {
                insert.put("k", "so");
                insert.put("v", inputPlan.get("SO Number"));
                insert.put("d", "SO Number");
            } else {
                insert.put("v", value != null ? value : "");
            }

            planItem.add(insert);

        }

        return planItem.isEmpty() ? null : planItem;
    }
}
