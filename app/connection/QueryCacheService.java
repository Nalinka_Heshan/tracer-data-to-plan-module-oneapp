package connection;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.HashMap;

@Singleton
public class QueryCacheService {
    protected HashMap<String, MongoDatabase> databaseMap;
    private HashMap<String, HashMap<String, Document>> queryCacheMap = new HashMap<>();

    @Inject
    public QueryCacheService(Mongo mongo){
        this.databaseMap = mongo.databaseMap;

    }


    public Document getQuery(String subjectKey, String queryName) {
        HashMap<String, Document> queryForSubjectKey = getQueryForSubjectKey(subjectKey);
        if (queryForSubjectKey == null || queryForSubjectKey.get(queryName)==null){
            HashMap<String, Document> CommonQueries = getQueryForSubjectKey("*");
            return CommonQueries != null ? CommonQueries.get(queryName) : null;
        }
        return queryForSubjectKey.get(queryName);
    }

    public void resetQueryCache(String subjectKey) {
        if (subjectKey.equalsIgnoreCase("*")) {
            this.queryCacheMap = new HashMap<>();
        } else {
            this.queryCacheMap.remove(subjectKey);
        }
    }

    private HashMap<String, Document> getQueryForSubjectKey(String subjectKey) {
        if (queryCacheMap.get(subjectKey) == null) {
            this.loadQueryCacheForSubjectKey(subjectKey);
        }
        return queryCacheMap.get(subjectKey);
    }

    private void loadQueryCacheForSubjectKey(String subjectKey) {
        this.queryCacheMap.put(subjectKey, getAllQuery(subjectKey, this.createQueryCollectionName(subjectKey)));
    }


    private String createQueryCollectionName(String subjectKey) {
        if (subjectKey.equalsIgnoreCase("*")){
            return "query";
        }
        subjectKey = subjectKey.replace('-', '_');
        return subjectKey + "_" + "query";
    }
    public HashMap<String, Document> getAllQuery(String subjectKey, String collectionName) {
        HashMap<String, Document> queryMap = new HashMap<>();
        MongoCollection<Document> collection = this.getDatabase(Mongo.DB_MASTERDATA).getCollection(collectionName);
        Document filter = new Document("subject_key", subjectKey);

        MongoCursor<Document> cursor = collection.find(filter).iterator();
        try {
            while (cursor.hasNext()) {
                Document document = cursor.next();
                queryMap.put(document.getString("query_name"), document);
            }
        } finally {
            cursor.close();
        }
        return queryMap;
    }
    protected MongoDatabase getDatabase(String databaseName){
        return databaseMap.get(databaseName);
    }
}
