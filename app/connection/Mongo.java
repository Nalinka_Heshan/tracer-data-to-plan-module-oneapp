package connection;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

import javax.inject.Inject;
import java.util.HashMap;

public class Mongo {
    public static String DB_PLAN = "plan";
    public static String DB_MASTERDATA = "masterdata";

    public HashMap<String, MongoDatabase> databaseMap = new HashMap<>();

    public MongoClient mongoClient;

    private static Mongo mongo = null;

    @Inject
    private Mongo() {
        MongoClientURI connectionString = new MongoClientURI("mongodb://localhost:27017",this.getConnectionOptionBuilder());
        mongoClient = new MongoClient(connectionString);

        databaseMap.put(Mongo.DB_PLAN,mongoClient.getDatabase("plan"));
        databaseMap.put(Mongo.DB_MASTERDATA,mongoClient.getDatabase("masterdata"));

    }

    public void mongoClose(){
        mongoClient.close();
    }

    public static Mongo getInstance(){
        if (mongo == null){
            synchronized (Mongo.class){
                if (mongo ==  null){
                    mongo = new Mongo();
                }
            }
        }
        return mongo;
    }

    private MongoClientOptions.Builder getConnectionOptionBuilder(){
        MongoClientOptions.Builder mongoClientOptionBuilder = MongoClientOptions.builder()
                .connectTimeout(20000)
                .socketKeepAlive(true);
        return mongoClientOptionBuilder;
    }

    public MongoDatabase getDatabase(String databaseName){
        return databaseMap.get(databaseName);
    }
}
