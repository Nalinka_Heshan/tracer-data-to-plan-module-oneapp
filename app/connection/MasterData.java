package connection;

import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.bson.BsonRegularExpression;
import org.bson.*;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriterSettings;
import org.json.simple.JSONObject;


import java.util.*;

public class MasterData {
    protected HashMap<String, MongoDatabase> databaseMap;
    QueryCacheService queryCacheService;
    public MasterData(Mongo mongo){
        this.databaseMap = mongo.databaseMap;
        queryCacheService= new QueryCacheService(mongo);
    }

    public JSONObject getMasterData(String subjectKey, String queryName, Map<String, Object> parameterMap) {
        JSONObject masterdataOutput = new JSONObject();
        try {
            Document queryConf = queryCacheService.getQuery(subjectKey, queryName);
            if (queryConf != null) {
                String collection = getCollectionNameForQuery(queryConf,subjectKey);
                String database = queryConf.get("database") != null ? queryConf.getString("database") : Mongo.DB_MASTERDATA;
                Document queryOutput = new Document();

                ArrayList<String> aggregateQueryStructure = (ArrayList<String>) queryConf.get("query");
                List<Document> aggregate = buildAggregation(aggregateQueryStructure, parameterMap);
                queryOutput = execAggregate(database, collection, aggregate);

                if (queryOutput.get("status").equals("success")) {
                    masterdataOutput.put("result", queryOutput.get("result"));
                }
            } else {
                String err = "this query not exist... plz check the query collection in DB or reset query cache. query_name : " + queryName + " is invalid";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return masterdataOutput;
    }
    private List<Document> buildAggregation(ArrayList<String> queryStructure, Map<String, Object> parameterMap) {
        List<Document> aggregate = new ArrayList<>();
        Set<String> keys = parameterMap.keySet();

        for (String q : queryStructure) {
            Document queryDoc = Document.parse(q);

            for (String key : keys) {
                String find = "<<-" + key + "->>";
                queryDoc = replaceDocValues(queryDoc, find, parameterMap.get(key));
            }
            aggregate.add(queryDoc);
        }
        return aggregate;
    }
    public Document execAggregate(String dbName, String collectionName, List<Document> aggregate) {
        Document queryOutput = new Document();
        MongoDatabase database = this.getDatabase(dbName);
        if (database != null) {
            MongoCollection<Document> collection = database.getCollection(collectionName);
            ArrayList<Document> result = new ArrayList<>();
            AggregateIterable<Document> itrDoc = collection.aggregate(aggregate);
            Iterator<Document> iterator = itrDoc.iterator();
            while (iterator.hasNext()) {
                Document document = iterator.next();
                result.add(document);
            }
            queryOutput.put("status", "success");
            queryOutput.put("result", result);

        }
        return queryOutput;
    }
    private String getCollectionNameForQuery(Document queryConf,String subjectKey){
        String collectionName = queryConf.get("collection").toString();
        if (collectionName.contains("<<-subject_key->>")){
            collectionName = collectionName.replace("<<-subject_key->>",subjectKey.replace("-","_"));
        }
        return collectionName;
    }
    protected MongoDatabase getDatabase(String databaseName){
        return databaseMap.get(databaseName);
    }

    private Document replaceDocValues(Document document, String find, Object value) {
        Set<String> keys = document.keySet();
        for (String key : keys) {
            if (document.get(key).getClass().equals(Document.class)) {
                Document innerDoc = (Document) document.get(key);
                innerDoc = this.replaceDocValues(innerDoc, find, value);
                document.replace(key, innerDoc);

            } else if (document.get(key).getClass().equals(BsonRegularExpression.class)) {
                String newString = document.toJson(new JsonWriterSettings(JsonMode.SHELL)).replaceAll(find, value.toString());
                document = Document.parse(newString);
            } else {
                if (document.get(key).equals(find)) {
                    document.replace(key, value);
                } else if (document.get(key).toString().contains(find)) {
                    String newValue = document.get(key).toString().replaceAll(find, value.toString());
                    document.replace(key, newValue);
                }
            }
        }
        return document;
    }
}
